===============================================
Create, configure  and start the device servers
===============================================

Create DS
=========

Configuration Manager
---------------------

tango::

  $ fandango add_new_device hdb++cm-srv/hdbpp-cm HdbConfigurationManager archiving/hdbpp/cm  

Event Subscriber Archiver
-------------------------

TODO: There is not a rule to organize the clients yet and in which archiver register the attribute. One possible solution is:

The event subcriber archiver DS are organized in different categories: Vacuum, Beamline, etc. They can have more than one device:

This properties must be set for HdbEventSubscriber Class::

  DefaultContext = RUN 
  SubscribeChangeAsFallback = True
  StartArchivingAtStartup = True

Add devices::

  $  fandango add_new_device hdb++es-srv/hdbpp-es-vc HdbEventSubscriber archiving/hdbpp/es-eps
  $  fandango add_new_device hdb++es-srv/hdbpp-es-vc HdbEventSubscriber archiving/hdbpp/es-vc

Run script on python::

  import fandango as fn
  devs = map(fn.tango.get_full_name,fn.tango.get_class_devices('HdbEventSubscriber'))
  print(devs)
  fn.put_device_property('archiving/hdbpp/cm','ArchiverList',devs)
  fn.put_device_property('archiving/hdbpp/cm','DbName','hdbpp')
  libconf = ['libname=/usr/lib/x86_64-linux-gnu/libhdb++mysql.so.6',  'dbname=hdbpp',      'user=manager',  'host=localhost',  'password=manager',  'port=3306',  'lightschema=1']
  fn.put_device_property('archiving/hdbpp/cm','LibConfiguration',libconf)
  [fn.put_device_property(d,'LibConfiguration',libconf) for d in devs]

Periodic Archiver
-----------------

TODO: There are not rules to organize the clients and in which archiver register the attribute. One possible solution is:

The periodic archiver DS are organized in different categories: Vacuum, Beamline, etc. They can have more than one device:

Add devices::

  $ fandango add_new_device PyHdbppPeriodicArchiver/hdbpp-per-vc PyHdbppPeriodicArchiver  archiving/hdbpp/per-eps  

  $ fandango add_new_device PyHdbppPeriodicArchiver/hdbpp-per-vc PyHdbppPeriodicArchiver  archiving/hdbpp/per-vc

Run script on python:

  import fandango as fn
  devs = map(fn.tango.get_full_name,fn.tango.get_class_devices('PyHdbppPeriodicArchiver'))
  print(devs)
  fn.put_device_property('archiving/hdbpp/cm','PeriodicArchivers',devs)
  for d in devs:
    fn.put_device_property(d,'ConfigurationManagerDS', 'archiving/hdbpp/cm')

Configure Starter
=================

Open jive
Add to the starter tango/admin/<hostname>  the property::

  StartDsPath:
    /usr/bin
    /usr/lib/tango

Restart the starter:
  sudo systemctl stop tango-starter
  sudo systemctl start tango-starter

Configure HDBpp property
========================

Open jive

Create a free property (Menu Edit): PyTangoArchiving
On the free property create new properties::

  Schemas:
    hdbpp
  hdbpp:
    reader=PyTangoArchiving.hdbpp.HDBpp('hdbpp','<host name>.cells.es', ...)
    api=PyTangoArchiving.hdbpp.HDBpp
    check=True
    method=get_attribute_values
    db_name=hdbpp
    user=...
    passwd=...
    host=...

  DbSchemas: # This property is needed to work with the PyTangoArchiving multi.starts method
    hdbpp
  DbConfig: # This property is needed when there is only one database
    archiver:archiver@<host name>/hdbpp




Start Servers and set levels::

  $ tango_servers <host name> start "hdb*es*/*" 2 
  $ tango_servers <host name> start "PyHdb*/*" 3
  $ tango_servers <host name> start "hdb*cm*/*" 4

Open astor and configure startup levels:


add attributes to hdb++
=======================

from python::

  In [2]: import fandango as fn, PyTangoArchiving as pta
  In [3]: hdbpp = pta.api('hdbpp')
  In [4]: hdbpp.add_periodic_attribute('sys/tg_test/1/short_scalar',period=3000)
  In [5]: hdbpp.add_attribute('sys/tg_test/1/double_scalar',code_event=True)
  ... wait some minutes or exit/enter ipython ...

  In [6]: hdbpp.load_last_values('sys/tg_test/1/short_scalar',n=10)
  In [7]: hdbpp.load_last_values('sys/tg_test/1/double_scalar')
